--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cart; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cart (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    total integer NOT NULL,
    quantity integer NOT NULL,
    user_id uuid,
    item_id uuid
);


ALTER TABLE public.cart OWNER TO postgres;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    category character varying NOT NULL
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menu (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    menu character varying NOT NULL,
    price integer NOT NULL,
    description character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    category_id uuid
);


ALTER TABLE public.menu OWNER TO postgres;

--
-- Name: payment_method; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_method (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    type character varying NOT NULL,
    instruction character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at time with time zone
);


ALTER TABLE public.payment_method OWNER TO postgres;

--
-- Name: review_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.review_user (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    message character varying NOT NULL,
    transaction_id uuid
);


ALTER TABLE public.review_user OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    role_type character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    version integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: transactions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transactions (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    payment_status boolean DEFAULT false NOT NULL,
    delivery_status character varying NOT NULL,
    image character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at time with time zone,
    method_id uuid,
    user_id uuid,
    sub_total integer NOT NULL
);


ALTER TABLE public.transactions OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    phone character varying NOT NULL,
    full_name character varying NOT NULL,
    address character varying,
    image character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    version integer DEFAULT 1 NOT NULL,
    roles_id integer,
    transaction_id uuid
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Data for Name: cart; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cart (id, total, quantity, user_id, item_id) FROM stdin;
\.


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, category) FROM stdin;
729006e4-0a9b-40a5-8e65-e0bab714c1ac	Main Menu
c4bef283-49dd-4081-9c61-88a45666495f	Snack
\.


--
-- Data for Name: menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menu (id, menu, price, description, created_at, updated_at, deleted_at, category_id) FROM stdin;
d7ac105c-af45-4eb2-a458-dc01bbea04df	Black Burger	5000	wailah enak bang	2022-08-03 12:46:09.983841+07	2022-08-03 14:55:07.896121+07	\N	c4bef283-49dd-4081-9c61-88a45666495f
\.


--
-- Data for Name: payment_method; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_method (id, type, instruction, created_at, updated_at, deleted_at) FROM stdin;
4103228d-c74a-4b1a-adb2-f6fd8c96d1fc	BCA	1. Masukkan kartu ATM dan PIN kamu. 2. Masuk ke menu TRANSFER dan klik BCA Virtual Account. 3. Masukkan Nomor Rekening kami: 7000180124231. 4. Masukkan jumlah sesuai dengan nominal pemesanan. 5. Ikuti petunjuk berikutnya untuk menyelesaikan pembayaran	2022-07-19 17:49:07.369248+07	2022-07-19 17:49:07.369248+07	\N
6590bd1f-dfc0-472e-9b95-5a7f945591ea	BRI	1. Masukkan kartu ATM dan PIN kamu. 2. Masuk ke menu TRANSFER dan klik BRI Virtual Account. 3. Masukkan Nomor Rekening kami: 7214180124231. 4. Masukkan jumlah sesuai dengan nominal pemesanan. 5. Ikuti petunjuk berikutnya untuk menyelesaikan pembayaran	2022-07-19 17:50:12.327377+07	2022-07-19 17:50:12.327377+07	\N
b5b010bb-aee9-4b2f-8c64-56807bfbb380	Dana	Kirim ke nomor 089121321	2022-07-28 12:15:12.984718+07	2022-07-28 12:15:12.984718+07	\N
\.


--
-- Data for Name: review_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.review_user (id, message, transaction_id) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, role_type, created_at, updated_at, deleted_at, version) FROM stdin;
1	Admin	2022-07-12 11:26:07.755916+07	2022-07-12 11:26:07.755916+07	\N	1
2	Member	2022-07-12 11:35:36.08108+07	2022-07-12 11:35:36.08108+07	\N	1
\.


--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transactions (id, payment_status, delivery_status, image, created_at, updated_at, deleted_at, method_id, user_id, sub_total) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, email, password, phone, full_name, address, image, created_at, updated_at, deleted_at, version, roles_id, transaction_id) FROM stdin;
7176feb8-8695-4f7f-b517-565bdcf72cbe	faizalll@mail.com	$2b$10$8NG6kljhNruKgU9t8xlSd.PBlQYlauJ6cqvoXwJTgpdn/BA6xleoG	0812345	Faizal Aldiansyah	\N	\N	2022-07-29 17:04:07.443328+07	2022-07-29 17:04:07.443328+07	\N	1	2	\N
dea2e334-3685-4fb0-b33e-525cfb530e5d	faizal@gmail.com	$2b$10$h9FmsIXEdSaDqVr5Bsg0kOlg3ZVZDLf/p9QL5TIJL4T7O1yACQe7q	08565656	faizal	\N	\N	2022-07-29 17:05:50.444885+07	2022-07-29 17:05:50.444885+07	\N	1	2	\N
87a71b8b-6308-4882-9c73-c9d997265833	ucilgemink@gmail.com	$2b$10$dGFevk9YHvhqqn6x9T3VbeMb3chJrbZr5l54alU6oenXckrhyvZSG	089657393880	ucilgemink	\N	\N	2022-07-29 17:10:00.182982+07	2022-07-29 17:10:00.182982+07	\N	1	2	\N
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 4, true);


--
-- Name: menu PK_35b2a8f47d153ff7a41860cceeb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT "PK_35b2a8f47d153ff7a41860cceeb" PRIMARY KEY (id);


--
-- Name: payment_method PK_7744c2b2dd932c9cf42f2b9bc3a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_method
    ADD CONSTRAINT "PK_7744c2b2dd932c9cf42f2b9bc3a" PRIMARY KEY (id);


--
-- Name: review_user PK_81446f2ee100305f42645d4d6c2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.review_user
    ADD CONSTRAINT "PK_81446f2ee100305f42645d4d6c2" PRIMARY KEY (id);


--
-- Name: category PK_9c4e4a89e3674fc9f382d733f03; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY (id);


--
-- Name: transactions PK_a219afd8dd77ed80f5a862f1db9; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" PRIMARY KEY (id);


--
-- Name: roles PK_c1433d71a4838793a49dcad46ab; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT "PK_c1433d71a4838793a49dcad46ab" PRIMARY KEY (id);


--
-- Name: cart PK_c524ec48751b9b5bcfbf6e59be7; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cart
    ADD CONSTRAINT "PK_c524ec48751b9b5bcfbf6e59be7" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: review_user REL_bcc0a1d18dacaed6a6653cc5b5; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.review_user
    ADD CONSTRAINT "REL_bcc0a1d18dacaed6a6653cc5b5" UNIQUE (transaction_id);


--
-- Name: user UQ_e12875dfb3b1d92d7d7c5377e22; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email);


--
-- Name: user UQ_e36b77a5263ac0f191277c4c5d2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e36b77a5263ac0f191277c4c5d2" UNIQUE (transaction_id);


--
-- Name: transactions UQ_e9acc6efa76de013e8c1553ed2b; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT "UQ_e9acc6efa76de013e8c1553ed2b" UNIQUE (user_id);


--
-- Name: menu FK_246dfbfa0f3b0a4e953f7490544; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT "FK_246dfbfa0f3b0a4e953f7490544" FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- Name: user FK_8acd5cf26ebd158416f477de799; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "FK_8acd5cf26ebd158416f477de799" FOREIGN KEY (roles_id) REFERENCES public.roles(id);


--
-- Name: transactions FK_968550da80238c229776382c544; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT "FK_968550da80238c229776382c544" FOREIGN KEY (method_id) REFERENCES public.payment_method(id);


--
-- Name: review_user FK_bcc0a1d18dacaed6a6653cc5b50; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.review_user
    ADD CONSTRAINT "FK_bcc0a1d18dacaed6a6653cc5b50" FOREIGN KEY (transaction_id) REFERENCES public.transactions(id);


--
-- Name: cart FK_bd94725aa84f8cf37632bcde997; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cart
    ADD CONSTRAINT "FK_bd94725aa84f8cf37632bcde997" FOREIGN KEY (item_id) REFERENCES public.menu(id);


--
-- Name: user FK_e36b77a5263ac0f191277c4c5d2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "FK_e36b77a5263ac0f191277c4c5d2" FOREIGN KEY (transaction_id) REFERENCES public.transactions(id);


--
-- Name: transactions FK_e9acc6efa76de013e8c1553ed2b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT "FK_e9acc6efa76de013e8c1553ed2b" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: cart FK_f091e86a234693a49084b4c2c86; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cart
    ADD CONSTRAINT "FK_f091e86a234693a49084b4c2c86" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- PostgreSQL database dump complete
--

