--
-- PostgreSQL database dump
--

-- Dumped from database version 14.3
-- Dumped by pg_dump version 14.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    category character varying NOT NULL
);


ALTER TABLE public.category OWNER TO postgres;

--
-- Name: payment_method; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payment_method (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    type character varying NOT NULL,
    instruction character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at time with time zone
);


ALTER TABLE public.payment_method OWNER TO postgres;

--
-- Name: plan_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.plan_menu (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    menu character varying NOT NULL,
    price integer NOT NULL,
    description character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    category_id uuid,
    transaction_id uuid
);


ALTER TABLE public.plan_menu OWNER TO postgres;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id integer NOT NULL,
    role_type character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    version integer DEFAULT 1 NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: transactions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transactions (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    status character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at time with time zone,
    user_id uuid,
    method_id uuid
);


ALTER TABLE public.transactions OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email character varying NOT NULL,
    role_id integer NOT NULL,
    password character varying NOT NULL,
    phone character varying NOT NULL,
    full_name character varying NOT NULL,
    address character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    version integer DEFAULT 1 NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category (id, category) FROM stdin;
2ced6b4e-d14e-420c-9b1a-949653aecb2a	Main Menu
1cef358e-5007-49ec-8364-9ef4d185b2c1	Side Menu
\.


--
-- Data for Name: payment_method; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payment_method (id, type, instruction, created_at, updated_at, deleted_at) FROM stdin;
4103228d-c74a-4b1a-adb2-f6fd8c96d1fc	BCA	1. Masukkan kartu ATM dan PIN kamu. 2. Masuk ke menu TRANSFER dan klik BCA Virtual Account. 3. Masukkan Nomor Rekening kami: 7000180124231. 4. Masukkan jumlah sesuai dengan nominal pemesanan. 5. Ikuti petunjuk berikutnya untuk menyelesaikan pembayaran	2022-07-19 17:49:07.369248+07	2022-07-19 17:49:07.369248+07	\N
6590bd1f-dfc0-472e-9b95-5a7f945591ea	BRI	1. Masukkan kartu ATM dan PIN kamu. 2. Masuk ke menu TRANSFER dan klik BRI Virtual Account. 3. Masukkan Nomor Rekening kami: 7214180124231. 4. Masukkan jumlah sesuai dengan nominal pemesanan. 5. Ikuti petunjuk berikutnya untuk menyelesaikan pembayaran	2022-07-19 17:50:12.327377+07	2022-07-19 17:50:12.327377+07	\N
\.


--
-- Data for Name: plan_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.plan_menu (id, menu, price, description, created_at, updated_at, deleted_at, category_id, transaction_id) FROM stdin;
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, role_type, created_at, updated_at, deleted_at, version) FROM stdin;
1	Admin	2022-07-12 11:26:07.755916+07	2022-07-12 11:26:07.755916+07	\N	1
2	Member	2022-07-12 11:35:36.08108+07	2022-07-12 11:35:36.08108+07	\N	1
4	Member	2022-07-12 14:37:53.783297+07	2022-07-12 14:37:53.783297+07	\N	1
\.


--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transactions (id, status, created_at, updated_at, deleted_at, user_id, method_id) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, email, role_id, password, phone, full_name, address, created_at, updated_at, deleted_at, version) FROM stdin;
425b6e15-6c2b-40e2-9b1b-0c610b81c457	faizal@mail.com	1	$2b$12$KLkEQDNOvLkKsnFqLepQiON6WYKOM4BmxjnojB1OTHrT0GbqBvmbC	0812345	Faizal Aldiansyah	Bogor	2022-07-15 17:36:36.817578+07	2022-07-15 17:36:36.817578+07	\N	1
3c335576-e22d-4254-ab05-330c0df49d99	nabil@mail.com	1	$2b$12$.fwFRC7jOQjyQan13tkK5uqbkustclGYqTvRqFjglyR/asFdMF6Xa	08123456	Nabil Lanten	Bojong	2022-07-15 17:37:32.812004+07	2022-07-18 12:35:20.580362+07	\N	2
5d5266e0-8829-47c6-ac05-b72f29b96c83	ariel@mail.com	2	$2b$12$Yo3FclHYoOmd1IROfa02R.s544wFHJWSQTxdoTTBTMNh2cAVckWpy	09812431543	Ariel Ashari	Depok	2022-07-18 14:06:49.756271+07	2022-07-18 14:06:49.756271+07	\N	1
acf3073d-e95b-4767-9a51-b3a27fa5a95f	dwi@mail.com	2	dwi12345	0812432112	Dwi Gunardi	Sukabapak	2022-07-18 18:04:06.380026+07	2022-07-18 18:04:06.380026+07	\N	1
\.


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 4, true);


--
-- Name: payment_method PK_7744c2b2dd932c9cf42f2b9bc3a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payment_method
    ADD CONSTRAINT "PK_7744c2b2dd932c9cf42f2b9bc3a" PRIMARY KEY (id);


--
-- Name: plan_menu PK_8d74f4ababf62feee4a3abcf270; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plan_menu
    ADD CONSTRAINT "PK_8d74f4ababf62feee4a3abcf270" PRIMARY KEY (id);


--
-- Name: category PK_9c4e4a89e3674fc9f382d733f03; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY (id);


--
-- Name: transactions PK_a219afd8dd77ed80f5a862f1db9; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT "PK_a219afd8dd77ed80f5a862f1db9" PRIMARY KEY (id);


--
-- Name: roles PK_c1433d71a4838793a49dcad46ab; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT "PK_c1433d71a4838793a49dcad46ab" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: user UQ_e12875dfb3b1d92d7d7c5377e22; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email);


--
-- Name: plan_menu FK_3771e0432fde231265e1c051587; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plan_menu
    ADD CONSTRAINT "FK_3771e0432fde231265e1c051587" FOREIGN KEY (category_id) REFERENCES public.category(id);


--
-- Name: plan_menu FK_67f951d251af6ee84d38b93ac8f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.plan_menu
    ADD CONSTRAINT "FK_67f951d251af6ee84d38b93ac8f" FOREIGN KEY (transaction_id) REFERENCES public.transactions(id);


--
-- Name: transactions FK_968550da80238c229776382c544; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT "FK_968550da80238c229776382c544" FOREIGN KEY (method_id) REFERENCES public.payment_method(id);


--
-- Name: transactions FK_e9acc6efa76de013e8c1553ed2b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT "FK_e9acc6efa76de013e8c1553ed2b" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: user FK_fb2e442d14add3cefbdf33c4561; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "FK_fb2e442d14add3cefbdf33c4561" FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- PostgreSQL database dump complete
--

