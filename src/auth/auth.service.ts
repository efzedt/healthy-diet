import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt'
import { jwtConstants } from './constants';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    @InjectRepository(User) private readonly userRepo: Repository<User>
  ) {}

  async validateUser(email: string, password: string){
    try{
    const user = await this.userRepo.findOneOrFail({where: {email: email}, relations: ['roles']})
    if (user){
      const matched = bcrypt.compareSync(password, user.password)
      
      if (matched){
        const { ...result } = user
        return result;
        
      } else {
        throw new UnauthorizedException();
      }
    }
    return null;
    } catch(e){
      console.log(e);
      
    }
      
  }
  


  async  generateToken(user:any){    
    console.log(user);
    
    return {
      access_token: this.jwtService.sign({
        id: user.id,
        email: user.email,
        name: user.fullName,
        role: user.roles.role_type,
        address: user.address,
        phone: user.phone,
      })
    }
  }
}
