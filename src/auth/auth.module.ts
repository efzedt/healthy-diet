import { forwardRef, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from 'src/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    forwardRef(()=> UsersModule), 
    TypeOrmModule.forFeature([User]),
    PassportModule,
    JwtModule.register({
      secret: 'agshfbsnbfdkjsbfdnjsk',
      signOptions: { expiresIn: '1d' }
    })
  ],
  controllers: [AuthController,],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [LocalStrategy]
})
export class AuthModule {}
