import { Body, ConflictException, Controller, Get, HttpStatus, Post, Request, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { Login } from 'src/users/dto/login.dto';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { LocalAuthGuard } from './local-auth.guard';
import { LocalStrategy } from './local.strategy';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService
    ) {}

    @UseGuards(LocalAuthGuard)
    @Post('login')
    login(@Request() req){
      return this.authService.generateToken(req.user)
    }

  @Post('register')
  async create(@Body() createUserDto: CreateUserDto){
    try{
      return {
        data: await this.usersService.create(createUserDto),
        statusCode: HttpStatus.CREATED,
        message: 'success',
      };
    } catch(e){
      console.log(e);
      
      if (e.code === '23505'){
        throw new ConflictException('Email is already exists');

      }
    }

  }
}
