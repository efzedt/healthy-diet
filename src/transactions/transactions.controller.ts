import { Body, Controller, DefaultValuePipe, Delete, Get, HttpStatus, Param, ParseIntPipe, ParseUUIDPipe, Patch, Post, Put, Query, Request, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { Pagination } from 'nestjs-typeorm-paginate';
import { extname } from 'path';
import { cwd } from 'process';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { ApproveDto } from './dto/approval.dto';
import { CheckoutDto } from './dto/checkout.dto';
import { ProofDto } from './dto/proof.dto';
import { Transactions } from './entities/transactions.entity';
import { TransactionsService } from './transactions.service';

@UseGuards(JwtAuthGuard)
@ApiTags("Transactions")
@Controller('transactions')
export class TransactionsController {
  constructor(
    private readonly transactionService: TransactionsService
  ){}
  
  @UseGuards(JwtAuthGuard)
  @Post()
  async createTransaction(
    @Body() checkoutDto: CheckoutDto,
    @Request() req,
  ){
    try{
      return{
        data: await this.transactionService.createTransaction(checkoutDto, req.user.id),
        statusCode: HttpStatus.CREATED,
        message: 'success',
      };
    } catch(e){
      console.log((e));

  }
  }

  @Get('checkout/:id')
  async getCheckout(@Param('id', ParseUUIDPipe) id: string){
    try{
      return{
        data: await this.transactionService.getCheckout(id)
      }
    } catch(e){
      console.log(e);
      
    }
    
  }

  @Get()
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<Transactions>>{
    try{
    return await this.transactionService.getAll(query)
    } catch(e){
      console.log(e);
      
    }

  }

  @Get('export/data')
  async export(){
    const filename = await this.transactionService.export();
    return {
      filename : `https://api.cannis-catering.online/file/export/download/${filename}`
    }
  }

  @Put('admin/:id')
  async approveAdmin(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() approveDto: ApproveDto,
  ) {
    return {
      data: await this.transactionService.approveAdmin(id, approveDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put('/:id')
  async uploadProof(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() proofDto: ProofDto,
  ) {
    return {
      data: await this.transactionService.uploadProof(id, proofDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    await this.transactionService.remove(id);
    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
