import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsNotEmpty, IsString, IsUUID } from "class-validator";


export class CheckoutDto{
    @ApiProperty()
    @IsUUID()
    menuId: string;

    @ApiProperty({default: false})
    @IsBoolean()
    paymentStatus: boolean;

    @ApiProperty()
    @IsString()
    deliveryStatus: string;

    @ApiProperty()
    @IsString()
    image?: string;
}