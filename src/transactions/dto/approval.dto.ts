import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsBoolean, IsString } from "class-validator";
import { CheckoutDto } from "./checkout.dto";


export class ApproveDto extends PartialType(CheckoutDto){
    @ApiProperty({default: false})
    @IsBoolean()
    paymentStatus?: boolean;

    @ApiProperty()
    @IsString()
    deliveryStatus?: string;
}