import { ApiProperty, PartialType } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { CheckoutDto } from "./checkout.dto";


export class ProofDto extends PartialType(CheckoutDto){
    @ApiProperty()
    @IsString()
    image?: string;

    @ApiProperty()
    @IsString()
    deliveryStatus?: string;
}