import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Menu } from 'src/product/entities/menu.entity';
import { MenuModule } from 'src/product/menu.module';
import { User } from 'src/users/entities/user.entity';
import { UsersModule } from 'src/users/users.module';
import { Transactions } from './entities/transactions.entity';
import { TransactionsController } from './transactions.controller';
import { TransactionsService } from './transactions.service';
import { ReviewService } from './review/review.service';
import { ReviewModule } from './review/review.module';
import { ReviewUser } from './entities/review.entity';
import { ScheduleModule } from '@nestjs/schedule';


@Module({
  imports: [
    forwardRef(()=> UsersModule),
    forwardRef(()=> MenuModule),
    forwardRef(()=> ReviewModule),
    TypeOrmModule.forFeature([Transactions, Menu, User, ReviewUser]),
    ScheduleModule.forRoot(),
    ReviewModule,
    ],
  controllers: [TransactionsController],
  providers: [TransactionsService, ReviewService],
  exports: [TransactionsService]
})
export class TransactionsModule {}
