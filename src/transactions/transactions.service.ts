import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { FilterOperator, paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { generateExcel } from 'src/helper/export_excel';
import { Menu } from 'src/product/entities/menu.entity';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { EntityNotFoundError, Repository } from 'typeorm';
import { ApproveDto } from './dto/approval.dto';
import { CheckoutDto } from './dto/checkout.dto';
import { ProofDto } from './dto/proof.dto';
import { Transactions } from './entities/transactions.entity';



@Injectable()
export class TransactionsService {
    constructor(
        @InjectRepository(Transactions) private transactionRepo: Repository<Transactions>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Menu) private readonly menuRepo: Repository<Menu>,
        private usersService : UsersService,
    ){}
    async createTransaction(checkoutDto: CheckoutDto, userId: string){
      const menuId: any = checkoutDto.menuId
      const users: any = userId
      const expDate = new Date(new Date().getTime() + (24 * 60 * 60 * 1000))
      const selectedPlan = await this.menuRepo.findOne({where: {id: menuId}})

      const order = new Transactions()
      order.user = users
      order.menu = menuId
      order.paymentStatus = checkoutDto.paymentStatus
      order.deliveryStatus = checkoutDto.deliveryStatus
      order.total = selectedPlan.price
      order.expDate = expDate

      const result = await this.transactionRepo.insert(order)
      return this.transactionRepo.findOneOrFail({
        where: {
          id: result.identifiers[0].id,
        }, 
      })
  }

  async getCheckout(transactionId: string){
    return await this.transactionRepo.findOne({where: {id: transactionId}, relations: ['user', 'menu']})
  }

  async getAll(query: PaginateQuery): Promise<Paginated<Transactions>>{
    return paginate(query, this.transactionRepo, {
      sortableColumns: ['createdAt'],
      searchableColumns: ['user.fullName', 'user.email'],
      defaultSortBy: [['createdAt', 'DESC']],
      defaultLimit: 5,
      relations: ['menu', 'user'],
      filterableColumns: {
        "menu.menu": [FilterOperator.EQ],
        paymentStatus: [FilterOperator.EQ]
      }
    })
  }

  async approveAdmin(transactionId: string, approveDto: ApproveDto){
    try {
      await this.transactionRepo.findOneOrFail({
        where: {
          id: transactionId,
        },
        withDeleted: true
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    const transaction = new Transactions()
    transaction.paymentStatus = approveDto.paymentStatus
    transaction.deliveryStatus = approveDto.deliveryStatus
    
    await this.transactionRepo.update(transactionId, transaction)
  }

  async uploadProof(transactionId: string, proofDto: ProofDto){
    try {
      await this.transactionRepo.findOneOrFail({
        where: {
          id: transactionId,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
    const transaction = new Transactions()
    transaction.image = proofDto.image
    transaction.deliveryStatus = proofDto.deliveryStatus


    if (transaction.deliveryStatus === "Delivered"){
      transaction.status = "Done"
    }
    
    
    await this.transactionRepo.update(transactionId, transaction)
  }

  async export(){
    const transaction = await this.transactionRepo.find({relations: ['user', 'menu']})

    return await generateExcel(transaction, 'dataTransaction')
  }

  async remove(id: string){
    try {
      await this.transactionRepo.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
    
    if (await this.transactionRepo.softDelete(id)){
      const transaction = new Transactions()
      transaction.status = "Cancelled"

      await this.transactionRepo.update(id, {status: "Cancelled"})
    }
  }

  @Cron(CronExpression.EVERY_SECOND)
  async validateApprovements(){
    const query = await this.transactionRepo.createQueryBuilder("transactions").select().getMany()
    
    const dateNow = new Date(new Date().getTime()) 
    const selectedFilter = query.filter(data => dateNow >= data.expDate && data.image == null && data.paymentStatus == false)
    
    
    
    const idFilter = selectedFilter.map(data => data.id)
    if (selectedFilter){
      idFilter.map(data => {
        this.transactionRepo.createQueryBuilder()
        .softDelete()
        .where("id = :id", {id: data})
        .execute()

        this.transactionRepo.createQueryBuilder()
        .withDeleted()
        .update()
        .set({
          status: "Cancelled"
        })
        .where("id = :id", {id: data})
        .execute()

        console.log(`status id ${idFilter} successfully changed to CANCELLED`);
        
      })
    }
  }
    
}
