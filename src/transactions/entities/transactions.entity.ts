import { Menu } from "src/product/entities/menu.entity";
import { User } from "src/users/entities/user.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Transactions{
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({
        default: false
    })
    paymentStatus: boolean;

    @Column()
    deliveryStatus: string;

    @Column({nullable: true})
    status: string;

    @Column()
    total: number

    @Column({nullable: true})
    image: string;

    @Column()
    expDate: Date;

    @ManyToOne(()=> User, user => user.id, {onDelete: "CASCADE"})
    user: User

    @ManyToOne(()=> Menu, menu => menu.id)
    menu: Menu;

    @CreateDateColumn({
        type: 'timestamp with time zone',
        nullable: false
    })
    createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamp with time zone',
        nullable: false
    })
    updatedAt: Date;

    @DeleteDateColumn({
        type: 'time with time zone',
        nullable: true
    })
    deletedAt: Date;
}