

export const dataUser = [
    {
        title: 'ID',
        dataIndex: 'id',
        width: 20
    },
    {
        title: 'Full Name',
        dataIndex: 'fullName',
        width: 20
    },
    {
        title: 'Email',
        dataIndex: 'email',
        width: 20
    },
    {
        title: 'Phone',
        dataIndex: 'phone',
        width: 20
    },
    {
        title: 'Address',
        dataIndex: 'address',
        width: 20
    },
    {
        title: 'Role',
        dataIndex: 'roles.role_type',
        width: 20
    },

]