

export const dataTransactions = [
    {
        title: 'ID',
        dataIndex: 'id',
        width: 20
    },
    {
        title: 'User',
        dataIndex: 'user.fullName',
        width: 20
    },
    {
        title: 'Menu',
        dataIndex: 'menu.menu',
        width: 20
    },
    {
        title: 'Status Payment',
        dataIndex: 'paymentStatus',
        width: 20
    },
    {
        title: 'Status Delivery',
        dataIndex: 'deliveryStatus',
        width: 20
    },
    {
        title: 'Total Price',
        dataIndex: 'total',
        width: 20
    },
    {
        title: 'Date',
        dataIndex: 'createdAt',
        width: 20
    },

]