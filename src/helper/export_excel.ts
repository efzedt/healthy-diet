import { excelInserter } from './excel_inserter';
import * as moment from 'moment';
import * as path from 'path';
import { HttpException, HttpStatus } from '@nestjs/common';
import { dataUser } from './dataUser';
import { dataTransactions } from './dataTransactions';
import { date } from 'joi';

export async function generateExcel(data, type) {
  try {
    const filename = `${moment().format('YYYYMMDDHHMM')}-${type}.xlsx`;
    const filepath = path.join(__dirname + '/../../uploads/export', filename);
    console.log(data,"ini datanya")
    await DataToExcel(filepath, data, type);
    return filename;
  }catch (e) {
    console.log(e,'generateExcel')
    throw new HttpException(
        {
          statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
          message: e.message,
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }

}

// const hitLogApiData = [
//   {
//     title: 'Time',
//     dataIndex: 'time',
//     width: 20,
//   },
//   {
//     title: 'Success',
//     dataIndex: 'success',
//     width: 20,
//   },
//   {
//     title: 'Failed',
//     dataIndex: 'failed',
//     width: 20,
//   },
//   {
//     title: 'Total',
//     dataIndex: 'total',
//     width: 20,
//   },
// ];



async function setDataHitLogApi(dataHitLogApi) {
  try {
    const dataFiltered = [];

    await dataHitLogApi.forEach((element) => {
      const filterElement = [];

      filterElement['id'] = element?.id;
      filterElement['fullName'] = element?.fullName;
      filterElement['email'] = element?.email;
      filterElement['phone'] = element?.phone;
      filterElement['address'] = element?.address;
      filterElement['roles.role_type'] = element?.roles.role_type;
      

      dataFiltered.push(filterElement);
    });

    return dataFiltered;
  } catch (e) {
    console.log(e, 'setDataHitLogAPI');
    throw new HttpException(
      {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: e.message,
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}

async function setDataHitTransaction(dataHitLogApi) {
  try {
    const dataFiltered = [];

    await dataHitLogApi.forEach((element) => {
      const filterElement = [];

      filterElement['id'] = element?.id;
      filterElement['user.fullName'] = element?.user.fullName;
      filterElement['menu.menu'] = element?.menu.menu;
      filterElement['paymentStatus'] = element?.paymentStatus;
      filterElement['deliveryStatus'] = element?.deliveryStatus;
      filterElement['total'] = element?.total;
      filterElement['createdAt'] =  moment(element?.createdAt).format();
      

      dataFiltered.push(filterElement);
    });

    return dataFiltered;
  } catch (e) {
    console.log(e, 'setDataHitLogAPI');
    throw new HttpException(
      {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: e.message,
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}

export async function DataToExcel(filepath, data, type) {
  try {
    if (type === 'dataUser') {
      const dataFiltered = await setDataHitLogApi(data);

      await excelInserter({
        filename: filepath,
        sheets: [
          {
            name: 'Data Hit Log Api',
            startRowFrom: 1,
            headers: dataUser,
            data: dataFiltered,
          },
        ],
      });
    } else if (type === 'dataTransaction') {
      const dataFiltered = await setDataHitTransaction(data);

      await excelInserter({
        filename: filepath,
        sheets: [
          {
            name: 'Data Transactions',
            startRowFrom: 1,
            headers: dataTransactions,
            data: dataFiltered,
          },
        ],
      });
    } else if (type === 'Customer-Register') {
      //kode yang sama tapi beda set header dan data nya
    } else if (type === 'Summary-Services') {
      //kode yang sama tapi beda set header dan data nya
    } else if (type === 'Top-Rate') {
      //kode yang sama tapi beda set header dan data nya
    }
  } catch (e) {
    console.log(e, 'dataToExcel');
    throw new HttpException(
      {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: e.message,
      },
      HttpStatus.INTERNAL_SERVER_ERROR,
    );
  }
}
