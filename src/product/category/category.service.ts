import { HttpException, HttpStatus, Injectable, NotFoundException, RequestMethod } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository } from 'typeorm';
import { CreateCategoryDto } from '../dto/create-category.dto';
import { UpdateCategoryDto } from '../dto/update-category.dto';
import { Category } from '../entities/category.entity';

@Injectable()
export class CategoryService {
    constructor(
        @InjectRepository(Category)
        private repository: Repository<Category>,
    ){}

    async create(CreateCategoryDto: CreateCategoryDto): Promise<Category>{
        const addCategory = CreateCategoryDto
        const category = this.repository.createQueryBuilder()
        .insert()
        .into(Category)
        .values(addCategory)
        .execute();

        return this.repository.findOneOrFail({
            where: {
                id: (await category).identifiers[0].id,
            },
        });
    }

    findAll(): Promise<Category[]> {
        return this.repository.find();
    }

    findOne(category: string): Promise<Category>{
        return this.repository.findOneOrFail({
            where: {
                category,
            }
        });
    }

    async update(id: string, updateCategoryDto: UpdateCategoryDto){
        try {
            await this.repository.findOneOrFail({
              where: {
                id,
              },
            });
          } catch (e) {
            if (e instanceof EntityNotFoundError) {
              throw new HttpException(
                {
                  statusCode: HttpStatus.NOT_FOUND,
                  error: 'Data not found',
                },
                HttpStatus.NOT_FOUND,
              );
            } else {
              throw e;
            }
          }
          await this.repository.update(id, updateCategoryDto);

    }
    

    async remove(id: string){
        const category = await this.findOne(id);
        return this.repository.remove(category);
    }
}
