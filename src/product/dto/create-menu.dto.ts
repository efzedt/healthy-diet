import { ParseUUIDPipe } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { IsInt, IsNotEmpty, isNumber, IsNumber, IsOptional, IsString, IsUUID } from "class-validator";


export class CreateMenuDto {
    @ApiProperty()
    @IsString()
    menu: string;

    @ApiProperty()
    @IsNotEmpty()
    price: number;

    @ApiProperty()
    @IsUUID()
    category_id: string;

    @ApiProperty()
    @IsString()
    description: string;

    @ApiProperty()
    @IsNotEmpty()
    image: string;

    @ApiProperty()
    @IsOptional()
    duration: string;
}
