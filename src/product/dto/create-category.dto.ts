import { IsString, IsUUID } from "class-validator";


export class CreateCategoryDto {
    @IsString()
    category: string;
}