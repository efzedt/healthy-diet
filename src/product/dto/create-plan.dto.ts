import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString, IsUUID } from "class-validator";


export class CreatePlanDto{
    @ApiProperty()
    @IsString()
    name: string;

    @ApiProperty()
    @IsUUID()
    menu1: string;

    @ApiProperty()
    @IsUUID()
    menu2: string;

    @ApiProperty()
    @IsUUID()
    menu3: string;

    @ApiProperty()
    @IsNotEmpty()
    price: number;
}