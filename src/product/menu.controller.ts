import { Body, Controller, DefaultValuePipe, Delete, Get, HttpStatus, InternalServerErrorException, Param, ParseIntPipe, ParseUUIDPipe, Post, Put, Query, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiQuery, ApiTags } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { extname } from 'path';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CreateMenuDto } from './dto/create-menu.dto';
import { CreatePlanDto } from './dto/create-plan.dto';
import { UpdateMenuDto } from './dto/update-menu.dto';
import { Menu } from './entities/menu.entity';
import { MenuService } from './menu.service';

@ApiTags("Menu")
@Controller('menu')
export class MenuController {
    constructor(private readonly menuService: MenuService) {}
    @UseGuards(JwtAuthGuard)
    @Post('create')
      async create(
        @Body() createMenuDto: CreateMenuDto,
        ) {
        try{
          return{
            data: await this.menuService.create(createMenuDto),
            statusCode: HttpStatus.CREATED,
            message: 'success',
          };
        } catch(e){
          console.log((e.code));

      }
    }

  
    @Get()
  async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<Menu>>{
    try{
    return await this.menuService.findAll(query)
    } catch(e){
      throw new InternalServerErrorException()
      
    }

  }
  
    @Get(':id')
      async findOne(@Param('id', ParseUUIDPipe) id: string) {
        return {
          data: await this.menuService.findOne(id),
          statusCode: HttpStatus.OK,
          message: 'success',
        };
      }
  
  
    @UseGuards(JwtAuthGuard)
    @Put('edit/:id')
      async update(
      @Param('id', ParseUUIDPipe) id: string,
      @Body() updateMenuDto: UpdateMenuDto,
    ) {
      console.log(updateMenuDto);
      
      return {
        data: await this.menuService.update(id, updateMenuDto),
        statusCode: HttpStatus.OK,
        message: 'success',
      };

    }

    @Get('search/:menu')
    async search(@Param('menu') menu: string){
      return await this.menuService.search(menu);
    }


    @UseGuards(JwtAuthGuard)
    @Delete(':id')
    async remove(@Param('id', ParseUUIDPipe) id: string) {
      await this.menuService.remove(id);
        return {
          statusCode: HttpStatus.OK,
          message: 'success',
      };
    }

}