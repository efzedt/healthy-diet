import { Transactions } from "src/transactions/entities/transactions.entity";
import { Column, CreateDateColumn, DeleteDateColumn, Entity, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, Unique, UpdateDateColumn, VersionColumn } from "typeorm";
import { Category } from "./category.entity";


@Entity()
export class Menu {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    menu: string;

    @Column()
    price: number;

    @Column({nullable: true})
    image: string

    @Column()
    description: string;

    @CreateDateColumn({
        type: 'timestamp with time zone',
        nullable: false,
      })
      createdAt: Date;
    
      @UpdateDateColumn({
        type: 'timestamp with time zone',
        nullable: false,
      })
      updatedAt: Date;
    
      @DeleteDateColumn({
        type: 'timestamp with time zone',
        nullable: true,
      })
      deletedAt: Date;

      @ManyToOne(()=> Category, category=>category.menu)
      category: Category

      @OneToMany(()=> Transactions, transaction => transaction.id)
      transaction: Transactions;

}