import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FilterOperator, paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { EntityNotFoundError, Like, Repository } from 'typeorm';
import { CategoryService } from './category/category.service';
import { CreateMenuDto } from './dto/create-menu.dto';
import { CreatePlanDto } from './dto/create-plan.dto';
import { UpdateMenuDto } from './dto/update-menu.dto';
import { Category } from './entities/category.entity';
import { Menu } from './entities/menu.entity';

@Injectable()
export class MenuService {
    constructor(
        @InjectRepository(Menu) private Repository: Repository<Menu>,
        @InjectRepository(Category) private readonly CategoryRepo: Repository<Category>,
        ) {}
    
    async create(createMenuDto: CreateMenuDto) {
        const categoryId: any = createMenuDto.category_id;
        const addMenu = new Menu
        addMenu.menu = createMenuDto.menu
        addMenu.price = createMenuDto.price
        addMenu.description = createMenuDto.description
        addMenu.image = createMenuDto.image
        addMenu.category = categoryId
        const result = await this.Repository.insert(addMenu)
    
        return this.Repository.findOneOrFail({
            where: {
                id: result.identifiers[0].id,
            },
        });
    }
    
    async findAll (query:PaginateQuery): Promise<Paginated<Menu>>{
        return paginate(query, this.Repository, {
          sortableColumns: ['price'],
          defaultSortBy: [['price', 'DESC']],
          defaultLimit: 5,
          searchableColumns: ['menu'],
          filterableColumns: {
            "category.category": [FilterOperator.EQ]
          },
          relations: ['category']
        })
      }
    
    async findOne(id: string) {
        try {
            return await this.Repository.findOneOrFail({
                where: {
                    id,
                },
            });
        } catch (e) {
            if (e instanceof EntityNotFoundError) {
                throw new HttpException({
                    statusCode: HttpStatus.NOT_FOUND,
                    error: 'Data not found',
                },
                HttpStatus.NOT_FOUND,
            );
            } else {
                throw e;
            }
        }
    }
    
    async update(id: string, updateMenuDto: UpdateMenuDto) {
        try {
          await this.Repository.findOneOrFail({
            where: {
              id,
            },
          });
        } catch (e) {
          if (e instanceof EntityNotFoundError) {
            throw new HttpException(
              {
                statusCode: HttpStatus.NOT_FOUND,
                error: 'Data not found',
              },
              HttpStatus.NOT_FOUND,
            );
          } else {
            throw e;
          }
        }
        
        const menu = new Menu()
        menu.menu = updateMenuDto.menu
        menu.price = updateMenuDto.price
        menu.description = updateMenuDto.description
        menu.image = updateMenuDto.image
        menu.category = await this.CategoryRepo.findOne({ where: {id: updateMenuDto.category_id} })

        await this.Repository.update(id, menu);
      }

      async search(menu: string){
        return await this.Repository.createQueryBuilder("menu")
        .where("menu.menu LIKE :menu", {menu: `%` + `${menu}` + `%`})
        .getMany()
      }

    async filterCategory(category: string){
        console.log(category)
        return await this.Repository.createQueryBuilder("menu")
        .leftJoinAndSelect("product.category", "category")
        .where("category.name = :category", {category: category})
        .getManyAndCount()        
    }


    async getOneMenu(id: string): Promise<Menu> {
        return this.Repository.findOneOrFail({where: {id: id}});
    }

    async remove(id: string) {
        try {
          await this.Repository.findOneOrFail({
            where: {
              id,
            },
          });
        } catch (e) {
          if (e instanceof EntityNotFoundError) {
            throw new HttpException(
              {
                statusCode: HttpStatus.NOT_FOUND,
                error: 'Data not found',
              },
              HttpStatus.NOT_FOUND,
            );
          } else {
            throw e;
          }
        }
    
        await this.Repository.delete(id);
      }
}
