import { forwardRef, Module } from '@nestjs/common';
import { MenuService } from './menu.service';
import { MenuController } from './menu.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Menu } from './entities/menu.entity';
import { Category } from './entities/category.entity';
import { CategoryModule } from './category/category.module';
import { TransactionsModule } from 'src/transactions/transactions.module';

@Module({
  imports: [TypeOrmModule.forFeature([Menu, Category]), CategoryModule, forwardRef(()=> TransactionsModule)],
  providers: [MenuService],
  controllers: [MenuController]
})
export class MenuModule {}
