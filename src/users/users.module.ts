import { forwardRef, Module } from '@nestjs/common';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User } from './entities/user.entity';
import { Roles } from './entities/roles.entity';
import { AuthModule } from 'src/auth/auth.module';
import { MulterModule } from '@nestjs/platform-express';
import { TransactionsModule } from 'src/transactions/transactions.module';
import { APP_GUARD } from '@nestjs/core';
import { Transactions } from 'src/transactions/entities/transactions.entity';


@Module({
  imports: [
    TypeOrmModule.forFeature([User, Roles, Transactions]), 
    forwardRef(()=> AuthModule), 
    MulterModule.register({dest: './uploads'}), 
    forwardRef(()=> TransactionsModule)],
  controllers: [UsersController],
  providers: [
    UsersService,
  ],
  exports: [UsersService]
})
export class UsersModule {}
