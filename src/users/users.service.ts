import { Body, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Code, EntityNotFoundError, Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Roles } from './entities/roles.entity';
import { retry } from 'rxjs';
import { Transactions } from 'src/transactions/entities/transactions.entity';
import { generateExcel } from 'src/helper/export_excel';
import * as bcrypt from 'bcrypt'
import { FilterOperator, paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { any } from 'joi';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Roles) private roleRepo: Repository<Roles>,
    @InjectRepository(Transactions) private transactionRepo: Repository<Transactions>
  ) {}

  async create(createUserDto: CreateUserDto) {
    const user= new User()
    user.email = createUserDto.email
    user.password= createUserDto.password
    user.fullName = createUserDto.fullName
    user.phone = createUserDto.phone
    user.roles = await this.roleRepo.findOneOrFail({where: {id: createUserDto.roleId}})

    const result = await this.usersRepository.insert(user)
      return this.usersRepository.findOneOrFail({
        where: {
          id: result.identifiers[0].id,
        }, 
      })
  }

  async findAll (query: PaginateQuery): Promise<Paginated<User>>{
    return paginate(query, this.usersRepository, {
      sortableColumns: ['fullName'],
      defaultSortBy: [['fullName', 'ASC']],
      searchableColumns: ['fullName', 'email'],
      defaultLimit: 5,
    })
  }

  async findOne(id: string) {
    try {
      return await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
  }

  async findEmail(email: string) {
    try {
        return await this.usersRepository.findOneOrFail({
            where: {
                email,
            },
            relations: ['roles']
        });
    } catch (e) {
        if (e instanceof EntityNotFoundError) {
            throw new HttpException({
                statusCode: HttpStatus.NOT_FOUND,
                error: 'Data not found',
            },
            HttpStatus.NOT_FOUND,
        );
        } else {
            throw e;
        }
    }
}

  async findName(fullName: string){
    return this.usersRepository.findOneOrFail({
      where:{ fullName },
    }
    )
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }
    // const salt = await bcrypt.genSalt()
    const user= new User()
    user.email = updateUserDto.email
    user.fullName = updateUserDto.fullName
    user.phone = updateUserDto.phone
    user.image = updateUserDto.image
    user.address = updateUserDto.address
    
    // if (updateUserDto.password){
    //   user.password = await bcrypt.hash(updateUserDto.password, salt)
    // }
    
    await this.usersRepository.update(id, user);
  }

  async remove(id: string) {
    try {
      await this.usersRepository.findOneOrFail({
        where: {
          id,
        },
      });
    } catch (e) {
      if (e instanceof EntityNotFoundError) {
        throw new HttpException(
          {
            statusCode: HttpStatus.NOT_FOUND,
            error: 'Data not found',
          },
          HttpStatus.NOT_FOUND,
        );
      } else {
        throw e;
      }
    }

    await this.usersRepository.delete(id);
  }

  async search(name: string){
    return await this.usersRepository.createQueryBuilder("user")
    .where("user.full_name LIKE :name", {name: `%` + `${name}` + `%`})
    .getMany()
  }

  async history(userId: string, query: PaginateQuery){
    const users: any = userId
    return paginate(query, this.transactionRepo, {
      sortableColumns: ['createdAt'],
      defaultSortBy: [['createdAt', 'DESC']],
      defaultLimit: 5,
      filterableColumns: {
        status: [FilterOperator.EQ]
      },
      relations: ['user', 'menu'],
      where: {user: users},
      withDeleted: true,
    })
  }

  async export(){
    const dataUser = await this.usersRepository.find({relations: ['roles']})

    return generateExcel(dataUser, 'dataUser');
  }
}
