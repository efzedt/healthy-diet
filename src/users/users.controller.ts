import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpStatus,
  UseGuards,
  Req,
  Request,
  HttpException,
  ConflictException,
  UseInterceptors,
  UploadedFile,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
  Patch,
  Res,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Pagination } from 'nestjs-typeorm-paginate';
import { User } from './entities/user.entity';
import { profile } from 'console';
import { profileDto } from './dto/profile.dto';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import process, { cwd } from 'process';
import { Paginate, Paginated, PaginateQuery } from 'nestjs-paginate';
import { Transactions } from 'src/transactions/entities/transactions.entity';

@UseGuards(JwtAuthGuard)
@ApiTags('user')
@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService
    ) {}

    @Get()
    async getAll(@Paginate() query: PaginateQuery): Promise<Paginated<User>>{
      try{
      return await this.usersService.findAll(query)
      } catch(e){
        console.log(e);
        
      }
  
    }

  @Get(':id')
  async findOne(@Param('id', ParseUUIDPipe) id: string) {
    return {
      data: await this.usersService.findOne(id),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put(':id')
  async update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    try{
      return {
        data: await this.usersService.update(id, updateUserDto),
        statusCode: HttpStatus.OK,
        message: 'success',
      };
    } catch(e){
      console.log(e);
      
      if (e.code === '23505'){
        throw new ConflictException('Email is already exists');
      
    }
  }
  }

  @Delete(':id')
  async remove(@Param('id', ParseUUIDPipe) id: string) {
    try{
      await this.usersService.remove(id);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
    } catch(e){
      console.log(e);
      
    }
    
  }

  @Get('search/:name')
    async search(@Param('name') name: string){
      return await this.usersService.search(name);

  }


  @Get(':email')
  async findEmail(@Param('email') email: string) {
    return {
      data: await this.usersService.findEmail(email),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get('history/:id')
  async history(
    @Param('id', ParseUUIDPipe) user: string,
    @Paginate() query: PaginateQuery): Promise<Paginated<Transactions>>{
    try{
    return await this.usersService.history(user, query)
    } catch(e){
      console.log(e);
      
    }

  }

  @Get('export/data')
  async export(){
    const filename = await this.usersService.export();
    return {
      filename : `https://api.cannis-catering.online/file/export/download/${filename}`
    }
  }
}

