import { defaults } from 'joi';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    VersionColumn,
    CreateDateColumn,
    OneToOne,
    OneToMany,
  } from 'typeorm';
import { User } from './user.entity';
  
  @Entity()
  export class Roles {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    role_type: string;
  
    @CreateDateColumn({
      type: 'timestamp with time zone',
      nullable: false,
    })
    createdAt: Date;
  
    @UpdateDateColumn({
      type: 'timestamp with time zone',
      nullable: false,
    })
    updatedAt: Date;
  
    @DeleteDateColumn({
      type: 'timestamp with time zone',
      nullable: true,
    })
    deletedAt: Date;
  
    @VersionColumn({default: 1})
    version: number;

    @OneToMany(()=>User, user=>user.roles)
    user: User
  }
  