import { Transactions } from 'src/transactions/entities/transactions.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  VersionColumn,
  CreateDateColumn,
  OneToOne,
  OneToMany,
  ManyToOne,
  BeforeInsert,
  BeforeUpdate,
  JoinColumn,
} from 'typeorm';
import * as bcrypt from 'bcrypt'
import { Roles } from './roles.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    unique: true
  })
  email: string;


  @Column()
  password: string;

  @Column()
  phone: string;

  @Column()
  fullName: string;

  @Column({nullable: true})
  address: string;

  @Column({nullable: true})
  image: string;

  @CreateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp with time zone',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp with time zone',
    nullable: true,
  })
  deletedAt: Date;

  @VersionColumn({default: 1})
  version: number;

  @ManyToOne(()=>Roles)
    roles: Roles;


  @BeforeInsert()
  async setPassword(password: string) {
    const salt = bcrypt.genSaltSync()
    this.password = bcrypt.hashSync(password || this.password, salt);
}

  @BeforeInsert()
  async emailToLowerCase(){
    this.email = this.email.toLowerCase();
  }

}
